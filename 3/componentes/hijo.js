Vue.component('hijo', {
    template: //html
    `
    <div class="py-5 bg-success">
        <h4>Componente Hijo: {{numero}}</h4>
        <h4>Nombre: {{nombre}}</h4>
        <button @click="numPadre++">+</button>
    </div>
    `,
    props: ['numero'],
    data() {
        return {
            nombre: 'Ignacio',
            numPadre: 0
        }
    },
    mounted (){
        this.$emit('nombreHijo', this.nombre);
    },
    updated(){
        this.$emit('numeroPadre', this.numPadre);
    }

})