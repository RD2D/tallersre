Vue.component('padre', {
    template: //html
    `
    <div class="p-5 bg-dark text-white">
        <h2>Componente Padre: {{numeroPadre}}</h2>
        <button class="btn-danger text-white" @click="numeroPadre++">+</button>
        {{nombrePadre}}
        <hijo :numero="numeroPadre" @nombreHijo="nombrePadre = $event" @numeroPadre="numeroPadre = $event"></hijo>
    </div>
    `,
    data(){
        return {
            numeroPadre: 0,
            nombrePadre: ''
        }
    }
});